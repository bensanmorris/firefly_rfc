# Features

* Specify list of plugin directories
* Ability to override plugins

# Editor changes

* Add Plugin list based UI with Add, Remove path (with relative / absolute path support)
* UI should display plugin name, URL, version, enable / disable

# Engine changes

* Add support for settings list of plugin paths as part of a game / scene
* Ability to launch editor in safe mode with plugins disabled

# Tests

* Plugin path added (absolute)
* Plugin path added (relative)
* Override plugin

